package dataaccess;
import itpm.KnowledgeArea;
import itpm.ProjectTODOMerge;
import itpm.QueryFormat;
import itpm.TimeManagement;
import itpm.dao.DAOProjectTODOMerge;
import at.tuvienna.interfaces.DAO;
import at.tuvienna.interfaces.DAOFactory;
import at.tuvienna.umlTUJava.UMLtuException;

public class HelperKnowledge {	
	
	public KnowledgeArea readAndCreateKnowledgeAreaOnDemand(Class<? extends KnowledgeArea> clazz) 
	throws UMLtuException {
		DAO<Object,String> dao = DAOFactory.create(clazz);
		KnowledgeArea bean = (KnowledgeArea)DAOFactory.createBean(clazz);
		bean.setKnowledgeAreaName(TimeManagement.class.getSimpleName());
		KnowledgeArea persistentBean = (KnowledgeArea)dao.readRecord(bean.getKnowledgeAreaName());
		if(persistentBean==null) {
			dao.insert(bean);
			System.out.println("INSERTED "+clazz.getSimpleName() + " * for the very first * time.");
			return bean;
		} else {
			return persistentBean;
		}
	}
	
	public QueryFormat readAndCreateRuleFormatOnDemand(Class<? extends QueryFormat> clazz) 
	throws UMLtuException {
		DAO<QueryFormat,String> dao = DAOFactory.create(clazz);
		QueryFormat bean = (QueryFormat)DAOFactory.createBean(clazz);
		QueryFormat persistentBean = dao.readRecord(bean.getId());
		if(persistentBean==null) {
			dao.insert(bean);
			System.out.println("INSERTED QueryFormat  * for the very first * time.");
			return bean;
		} else {
			return persistentBean;
		}
	}
	
	public ProjectTODOMerge readAndCreateProjectOnDemand(String projectId) throws UMLtuException {
				DAO<ProjectTODOMerge,String> dao = new DAOProjectTODOMerge();
				ProjectTODOMerge bean = new ProjectTODOMerge();
				bean.setId(projectId);
				ProjectTODOMerge persistentBean = dao.readRecord(bean.getId());
				if(persistentBean==null) {
					dao.insert(bean);
					System.out.println("INSERTED PROJECT " + projectId + "  * for the very first * time.");
					return bean;
				} else {
					return persistentBean;
				}
			}
	
	
	
	
	public  static void main (String ... args) throws UMLtuException {
		new HelperKnowledge().readAndCreateKnowledgeAreaOnDemand(TimeManagement.class);
	}
}
