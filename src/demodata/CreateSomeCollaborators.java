package demodata;
import itpm.Collaborator;
import itpm.TimeEntry;
import itpm.TimeSheet;
import itpm.dao.DAOCollaborator;
import itpm.dao.DAOTimeEntry;
import itpm.dao.DAOTimeSheet;

import java.util.GregorianCalendar;

import at.tuvienna.interfaces.DAO;
import at.tuvienna.umlTUJava.ManualTransaction;
import at.tuvienna.umlTUJava.UMLtuException;


public class CreateSomeCollaborators {
	public static void create() throws UMLtuException {
		ManualTransaction t = new ManualTransaction();
		DAO<Collaborator,String> daoC = new DAOCollaborator(t);
		DAO<TimeSheet,Integer> daoT = new DAOTimeSheet(t);
		
		t.begin();
		Collaborator agr = new Collaborator();
		TimeSheet tAgr = new TimeSheet();
		tAgr.setTimeSheetId(1);
		tAgr.setTrackWork(agr);
		agr.setCollaboratorId("agr");
		agr.setCollaboratorName("Andreas Gruenwald");
		agr.setCollaboratorDescription("Andreas Gruenwald, writting a master thesis.");
		agr.setCollaboratorEmail("a.gruenw@gmail.com");
		agr.setCollaboratorPhone("+436507782340");
		agr.setTrackWork(tAgr);
		daoT.insert(tAgr);
		daoC.insert(agr); //TODO insert and reflexiveness...
		t.commit();
		
	//	t.commit();
		t.begin();
		Collaborator sbi = new Collaborator();
		TimeSheet tSbi = new TimeSheet();
		tSbi.setTrackWork(sbi);
		tSbi.setTimeSheetId(2);
		sbi.setCollaboratorId("sbi");
		sbi.setCollaboratorName("Stefan Biffl");
		sbi.setCollaboratorEmail("stefan.biffl@tuwien.ac.at");
		sbi.setCollaboratorPhone("+4367611111");
		sbi.setTrackWork(tSbi);
		sbi.setCollaboratorDescription("Head of CDL-Flex");
		daoT.insert(tSbi);
		daoC.insert(sbi);
		t.commit();
		
		t.begin();
		Collaborator dwi = new Collaborator();
		TimeSheet tDwi = new TimeSheet();
		tDwi.setTimeSheetId(3);
		tDwi.setTrackWork(dwi);
		dwi.setCollaboratorId("dwi");
		dwi.setCollaboratorPhone("+4367611111");
		dwi.setCollaboratorName("Dietmar Winkler");
		dwi.setCollaboratorEmail("dietmar.winkler@tuwien.ac.at");
		dwi.setCollaboratorDescription("PM & QM expert at CDL-Flex.");
		dwi.setTrackWork(tDwi);
		daoT.insert(tDwi);
		daoC.insert(dwi);
		t.commit();
		
		
		/*
		t.begin();
		Collaborator rSbi = new Collaborator();
		rSbi.setCollaboratorId("sbi");
		daoC.remove(sbi);
		t.commit();
		*/
		
		//Add new time entry to agr
		t.begin();
		TimeEntry timeEntry = new TimeEntry();
		timeEntry.setTimeEntryId(1);
		timeEntry.setTimeEntryHours(2.5);
		timeEntry.setTimeEntryDescription("Working very hard");
		timeEntry.setTimeEntryDate(new GregorianCalendar(2013,01,27).getTime());
		agr.getTrackWork().getHasTimeSheetTimeEntryComposition().add(timeEntry); //take care to pass transaction!!!
		new DAOTimeEntry(t).insert(timeEntry);
		daoT.update(agr.getTrackWork());
		t.commit();
		
		//add a second time entry
		t.begin();
		TimeEntry timeEntry2 = new TimeEntry();
		timeEntry2.setTimeEntryId(2);
		timeEntry2.setTimeEntryHours(5.50);
		timeEntry2.setTimeEntryDescription("Working still harder");
		timeEntry2.setTimeEntryDate(new GregorianCalendar(2013,01,28).getTime());
		agr.getTrackWork().getHasTimeSheetTimeEntryComposition().add(timeEntry2); //take care to pass transaction!!!
		new DAOTimeEntry(t).insert(timeEntry2);
		daoT.update(agr.getTrackWork());
		t.commit();
		
		
		//add a thir time entry
		t.begin();
		TimeEntry timeEntry3 = new TimeEntry();
		timeEntry3.setTimeEntryId(3);
		timeEntry3.setTimeEntryHours(7.0);
		timeEntry3.setTimeEntryDescription("Dietmar is getting ready");
		timeEntry3.setTimeEntryDate(new GregorianCalendar(2013,01,29).getTime());
		dwi.getTrackWork().getHasTimeSheetTimeEntryComposition().add(timeEntry3); //take care to pass transaction!!!
		new DAOTimeEntry(t).insert(timeEntry3);
		daoT.update(dwi.getTrackWork());
		t.commit();
				
		
		
		
	}

}
