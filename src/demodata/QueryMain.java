package demodata;
import java.util.List;

import at.tuvienna.umlTUJava.UMLtuException;
import at.tuvienna.umlTUJava.query.OWLDLImpl;
import at.tuvienna.umlTUJava.query.QueryInterface;
import at.tuvienna.umlTUJava.query.SPARQLDLImpl;
import at.tuvienna.umlTUJava.query.SPARQLImpl;


public class QueryMain {

	/**
	 * @param args
	 * @throws UMLtuException 
	 */
	public static void main(String[] args) throws UMLtuException {
		QueryInterface queryInterface = new OWLDLImpl();
		
		String text = ("OWL DL Query Result for Collaborators: ");
		print(text, queryInterface.query("Collaborator"));
		
		String test = "SELECT * WHERE { ?x itpm:hasCollaboratorName ?n. " + 
									   "?x itpm:hasEmail ?m."+
									   "?x itpm:hasPhone ?p." + 
									   "?x itpm:hasIsExternal ?e." + 
									   "?x itpm:hasIsUnit ?u." +
									   "?x itpm:hasCollaboratorDescription ?d." +
									   "}";
		
		text = ("SPARQL (OWLAPI+JENA) Query Result for Collaborators: ");
		queryInterface = new SPARQLImpl();
		print(text, queryInterface.query(test));
		
		text = ("SPARQL-DL (OWLAPI+JENA) Query Result for Collaborators (order might be wrong): ");
		queryInterface = new SPARQLDLImpl();
		print(text, queryInterface.query(test));
	
	}
	
	private static void print(String text, List<List<String>> result) {
		System.out.println("********************************************************************************");
		System.out.println(String.format("**%75s**",text));
		System.out.println("********************************************************************************");
		int i = 0;
		for(List<String> x : result) {
			i++;
			System.out.print("Record " + i + ":");
			for(String n : x) {
				System.out.print(String.format("|%30s|",n.substring(0, Math.min(30, n.length()))));
			}
			System.out.println("");
		}
		System.out.println();
		System.out.println();
	}

}
