package demodata;
import at.tuvienna.owlapi.OwlAPITool;
import at.tuvienna.umlTUJava.UMLtuException;


public class InsertMain {

	/**
	 * @param args
	 * @throws UMLtuException 
	 */
	public static void main(String[] args) throws UMLtuException {
		OwlAPITool owlAPITool = new OwlAPITool();
		owlAPITool.dropAllIndividuals();
		try { 
			//CreateSomeCollaborators.create();
			CreateSomeRules.create();
		} catch (UMLtuException e) {
			//owlAPITool.dropAllIndividuals(); TODO add again
			throw e;
		}
		System.out.println("FINISHED WITHOUT ANY FAILURES.");
	}

}
