package demodata;
import itpm.HumanResourceManagement;
import itpm.ITPMQL;
import itpm.PMRule;
import itpm.ProjectTODOMerge;
import itpm.QueryFormat;
import itpm.SPARQL;
import itpm.ScopeManagement;
import itpm.dao.DAOPMRule;
import at.tuvienna.interfaces.DAO;
import at.tuvienna.umlTUJava.ManualTransaction;
import at.tuvienna.umlTUJava.UMLtuException;
import dataaccess.HelperKnowledge;


public class CreateSomeRules {
	public static void create() throws UMLtuException {
		ManualTransaction t = new ManualTransaction();
		DAO<PMRule,Integer> daoPMRule = new DAOPMRule(t);
		
		//get PROJECT DATA - TODO - UMl diagram anpassung
		ProjectTODOMerge project1 = new HelperKnowledge()
		.readAndCreateProjectOnDemand("project1");
		
		HumanResourceManagement hrm = (HumanResourceManagement)new HelperKnowledge()
			.readAndCreateKnowledgeAreaOnDemand(HumanResourceManagement.class);
		ScopeManagement scopeMgmt = (ScopeManagement)new HelperKnowledge()
			.readAndCreateKnowledgeAreaOnDemand(ScopeManagement.class);
		
		QueryFormat sparql = new SPARQL(); /* (QueryFormat)new HelperKnowledge()
		.readAndCreateRuleFormatOnDemand(SPARQL.class); //TODO solve ENUMS!!!!?? or leave it like this(?) */
		QueryFormat itpmQl = new ITPMQL();
		
		PMRule rule1 = new PMRule();
		rule1.setPMRuleId(1);
		rule1.setPMRuleName("Collaborator Working Hours per Week");
		rule1.setPMRuleDescription("The rule exemplifies the usage of SPARQL for visualization purposes.");
		String test1 = "SELECT " + 
				//"DISTINCT " +
				"?id ?n ?workingDate " +
				//"(SUM(?workingHours) as ?wh) " + 
				"(COALESCE(SUM(?workingHours), '0.0') AS ?notAvId) " + 
				//"(IF(bound(?wh), ?wh, 'Not available') as ?xx) "+
				//"(SUM(?workingHours) as ?hours) " + 
	      	   
				"WHERE { ?x itpm:hasCollaboratorName ?n. " + 
					   "?x itpm:hasCollaboratorId ?id." +
					   "?x itpm:hasCollaboratorEmail ?m."+
					   "?x itpm:hasCollaboratorPhone ?p." + 
					   "?x itpm:hasCollaboratorIsExternal ?e." + 
					   "?x itpm:hasCollaboratorIsUnit ?u." +
					   "?x itpm:hasCollaboratorDescription ?d." +
					   "?x itpm:trackWork ?timeSheet." +
					   "OPTIONAL { ?timeSheet itpm:hasTimeSheetTimeEntryComposition ?timeEntry ." + //Todo optional
					   "OPTIONAL { ?timeEntry itpm:hasTimeEntryDate ?workingDate . " + 
					   "		    ?timeEntry itpm:hasTimeEntryHours ?workingHours ." +
					   "FILTER (xsd:dateTime(?workingDate) >= '2013-02-26T00:00:00Z'^^xsd:dateTime " + 
					   	    "&& xsd:dateTime(?workingDate) <= '2013-03-01T23:59:59Z'^^xsd:dateTime) " +
					   "}}" + 
					   "} GROUP BY ?id ?n ?workingDate ORDER BY ASC(?n) " + 
					   "LIMIT 1000" + 
					   "OFFSET 0";
					   //"HAVING (xsd:dateTime(?workingDate) <= '2050-12-12T00:00:00Z'^^xsd:dateTime)"; // (day(?workingDate) AS ?day)";
					   //DESC(?day)
		rule1.setPMRuleRuleDefinition(test1);
		
		t.begin(); //TODO create and reuse transaction!?
		rule1.setFormat(sparql);
		rule1.setBelongsTo(hrm);
		rule1.setIsPMRuleOfProjectTODOMerge(project1);
		daoPMRule.insert(rule1);
		t.commit();
		
		//a second rule
		t.begin();
		PMRule rule2 = new PMRule();
		rule2.setPMRuleId(2);
		rule2.setPMRuleName("Collaborators existing.");
		rule2.setPMRuleDescription("This indeed is no rule but exemplifies a simple SPARQL query.");
		String test2 = "SELECT * WHERE { ?x itpm:hasCollaboratorName ?n. " + 
		   "?x itpm:hasCollaboratorEmail ?m."+
		   "?x itpm:hasCollaboratorPhone ?p." + 
		   "?x itpm:hasCollaboratorIsExternal ?e." + 
		   "?x itpm:hasCollaboratorIsUnit ?u." +
		   "?x itpm:hasCollaboratorDescription ?d." +
		   "}";
		rule2.setFormat(sparql);
		rule2.setIsPMRuleOfProjectTODOMerge(project1);
		rule2.setPMRuleRuleDefinition(test2);
		rule2.setBelongsTo(hrm);
		daoPMRule.insert(rule2);
		t.commit();	
		
		//and a third one second rule
		t.begin();
		PMRule rule3 = new PMRule();
		rule3.setPMRuleId(3);
		rule3.setPMRuleName("Collaborators existing.");
		rule3.setPMRuleDescription("This indeed is no rule but exemplifies a simple SPARQL query.");
		String test3 = "BacklogItem of Collaborator";
		rule3.setFormat(itpmQl);
		rule3.setIsPMRuleOfProjectTODOMerge(project1);
		rule3.setPMRuleRuleDefinition(test3);
		rule3.setBelongsTo(scopeMgmt);
		daoPMRule.insert(rule3);
		t.commit();	
		
	}

}
